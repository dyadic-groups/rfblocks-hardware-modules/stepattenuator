# A PE43711 Step Attenuator

## Features

- Insertion loss: 1.5dB for 9kHz to 1GHz, 2.0dB for 1 to 2GHz, 2.5dB for 2 to 4GHz.
- Error at 30dB attenuation of $\pm 0.5$ dB from 9kHz to 2.5GHz and
  $\pm 1.0$ dB from 9kHz to 4GHz.  Usable to 6GHz with increased
  attenuation error.
- Input 0.1dB compression: 31 dBm.
- Input IP3: 56 dBm.
- Choice of SMA or MMCX connectors.

## Documentation

Full documentation for the attenuator is available at
[RF Blocks](https://rfblocks.org/boards/PE43711-Step-Attenuator.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
